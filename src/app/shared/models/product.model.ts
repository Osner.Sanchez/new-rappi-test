export class Product {
    
    public id: string;
    public name: string;
    public quantity: number;
    public price: string;
    public available: boolean;
    public sublevel_id: number;

    constructor(product: Partial<Product>) {
        if (product) {
            this.id = product.id;
            this.name = product.name;
            this.quantity = product.quantity;
            this.price = product.price;
            this.available = product.available;
            this.sublevel_id = product.sublevel_id;
        }

    }
}