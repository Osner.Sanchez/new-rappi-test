export class Category {

    public id: string;
    public name: string;
    public sublevels?: Category[];

    constructor(category: Partial<Category>) {
        if (category) {
            this.id = category.id;
            this.name = category.name;
            this.sublevels = category.sublevels;
        }
    }

    public addSublevels(subleves: Category | Category[]) {
        this.sublevels ? this.sublevels : [];
        this.sublevels.concat(subleves);
    }
}