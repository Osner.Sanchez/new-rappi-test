
import { ActionReducerMap, MetaReducer } from '@ngrx/store';
import { environment } from 'src/environments/environment';

import * as reducers from './reducers'

export interface StateApp {
    products: reducers.ProductsState;
    cart: reducers.CartState;
    categories: reducers.CategoriesState;
}

export const reducersApp: ActionReducerMap<StateApp> = {
    products: reducers.productsReducer,
    cart: reducers.cartReducer,
    categories: reducers.categoriesReducer
};


export const metaReducers: MetaReducer<StateApp>[] = !environment.production ? [] : [];
