import { Action } from '@ngrx/store'
import { Product } from '../../models/product.model';

export const PROCCES_CART = '[Cart] Procesar Cart'
export const PROCCES_CART_FAIL = '[Cart] Procesar Fail Cart'
export const PROCCES_CART_SUCCES = '[Cart] Procesar Succes Cart'

export const LOAD_CART = '[Cart] Load Cart'
export const LOAD_CART_FAIL = '[Cart] Load Cart FAIL'
export const LOAD_CART_SUCCES = '[Cart] Load Cart SUCCESS'

export const ADD_PRODUCT_CART = '[Cart] Add product Cart'
export const ADD_PRODUCT_CART_FAIL = '[Cart] Add product Cart FAIL'
export const ADD_PRODUCT_CART_SUCCES = '[Cart] Add product Cart SUCCESS'

export const DELETE_PRODUCT_CART = '[Cart] DELETE product Cart'
export const DELETE_PRODUCT_CART_FAIL = '[Cart] DELETE product Cart FAIL'
export const DELETE_PRODUCT_CART_SUCCES = '[Cart] DELETE product Cart SUCCESS'

export class ProccesCart implements Action {
    readonly type: string = PROCCES_CART;
}

export class ProccesCartFail implements Action {
    readonly type: string = PROCCES_CART_FAIL;
    constructor(public payload: any) { }
}

export class ProccesCartSuccess implements Action {
    readonly type: string = PROCCES_CART_SUCCES;
    constructor() { }
}

export class LoadCart implements Action {
    readonly type: string = LOAD_CART;
}

export class LoadCartFail implements Action {
    readonly type: string = LOAD_CART_FAIL;
    constructor(public payload: any) { }
}

export class LoadCartSuccess implements Action {
    readonly type: string = LOAD_CART_SUCCES;
    constructor(public cart: Product[]) { }
}

export class AddProductCart implements Action {
    readonly type: string = ADD_PRODUCT_CART;
    constructor(public product: Product) { }
}

export class AddProductCartFail implements Action {
    readonly type: string = ADD_PRODUCT_CART_FAIL;
    constructor(public payload: any) { }
}

export class AddProductCartSuccess implements Action {
    readonly type: string = ADD_PRODUCT_CART_SUCCES;
    constructor() { }
}

export class DeleteProductCart implements Action {
    readonly type: string = DELETE_PRODUCT_CART;
    constructor(public product: Product) { }
}

export class DeleteProductCartFail implements Action {
    readonly type: string = DELETE_PRODUCT_CART_FAIL;
    constructor(public payload: any) { }
}

export class DeleteProductCartSuccess implements Action {
    readonly type: string = DELETE_PRODUCT_CART_SUCCES;
    constructor() { }
}

export type CartActions = LoadCart |
    LoadCartFail |
    LoadCartSuccess |
    AddProductCart |
    AddProductCartFail |
    AddProductCartSuccess |
    DeleteProductCart |
    DeleteProductCartFail |
    DeleteProductCartSuccess;