import { Action } from '@ngrx/store'
import { Category } from '../../models/category.model';

export const LOAD_CATEGORIES = '[Categories] Load Categories'
export const LOAD_CATEGORIES_FAIL = '[Categories] Load Categories FAIL'
export const LOAD_CATEGORIES_SUCCES = '[Categories] Load Categories SUCCESS'

export class LoadCategories implements Action {
    readonly type: string = LOAD_CATEGORIES;
}

export class LoadCategoriesFail implements Action {
    readonly type: string = LOAD_CATEGORIES_FAIL;
    constructor(public payload: any) { }
}

export class LoadCategoriesSuccess implements Action {
    readonly type: string = LOAD_CATEGORIES_SUCCES;
    constructor(public categories: Category[]) { }
}

export type CategoriesActions = LoadCategories |
                            LoadCategoriesFail |
                            LoadCategoriesSuccess;