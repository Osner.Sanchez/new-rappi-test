import { Action } from '@ngrx/store'
import { Product } from '../../models/product.model';

export const LOAD_PRODUCTS = '[Products] Load Products'
export const LOAD_PRODUCTS_FAIL = '[Products] Load Products FAIL'
export const LOAD_PRODUCTS_SUCCES = '[Products] Load Products SUCCESS'

export class LoadProducts implements Action {
    readonly type: string = LOAD_PRODUCTS;
    constructor(public filters?: any) { }
}

export class LoadProductsFail implements Action {
    readonly type: string = LOAD_PRODUCTS_FAIL;
    constructor(public payload: any) { }
}

export class LoadProductsSuccess implements Action {
    readonly type: string = LOAD_PRODUCTS_SUCCES;
    constructor(public products: Product[]) { }
}

export type ProductsActions = LoadProducts |
                            LoadProductsFail |
                            LoadProductsSuccess;