import { LOAD_CATEGORIES, LOAD_CATEGORIES_SUCCES, LOAD_CATEGORIES_FAIL, CategoriesActions } from "../actions";
import { Category } from '../../models/category.model';

export interface CategoriesState {
    categories: Category[];
    loaded: boolean;
    loading: boolean;
    error: any;
}

const initState: CategoriesState = {
    categories: [],
    loaded: false,
    loading: false,
    error: null
}

export function categoriesReducer(state = initState, action: CategoriesActions): CategoriesState {
    switch (action.type) {
        case LOAD_CATEGORIES:

            return {
                ...state,
                loading: true,
                error: null
            };
        case LOAD_CATEGORIES_FAIL:

            return {
                ...state,
                loading: false,
                loaded: false,
                error: (<any>action).payload
            };
        case LOAD_CATEGORIES_SUCCES:

            return {
                ...state,
                loading: false,
                loaded: true,
                categories: [...(<any>action).categories]
            };

        default:
            return state;
    }
}