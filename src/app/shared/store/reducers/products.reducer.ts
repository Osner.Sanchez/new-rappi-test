import { ProductsActions, LOAD_PRODUCTS, LOAD_PRODUCTS_SUCCES, LOAD_PRODUCTS_FAIL } from "../actions";
import { Product } from '../../models/product.model';

export interface ProductsState {
    products: Product[];
    filters: any;
    loaded: boolean;
    loading: boolean;
    error: any;
}

const initState: ProductsState = {
    products: [],
    filters: null,
    loaded: false,
    loading: false,
    error: null
}

export function productsReducer(state = initState, action: ProductsActions): ProductsState {
    switch (action.type) {
        case LOAD_PRODUCTS:

            return {
                ...state,
                filters: (<any>action).filters,
                loading: true,
                error: null
            };
        case LOAD_PRODUCTS_FAIL:

            return {
                ...state,
                loading: false,
                loaded: false,
                error: (<any>action).payload
            };
        case LOAD_PRODUCTS_SUCCES:

            return {
                ...state,
                loading: false,
                loaded: true,
                products: [...(<any>action).products]
            };

        default:
            return state;
    }
}