import { CartActions, LOAD_CART_SUCCES, LOAD_CART_FAIL, ADD_PRODUCT_CART, ADD_PRODUCT_CART_FAIL, ADD_PRODUCT_CART_SUCCES, LOAD_CART, DELETE_PRODUCT_CART, DELETE_PRODUCT_CART_SUCCES, DELETE_PRODUCT_CART_FAIL, PROCCES_CART, PROCCES_CART_SUCCES, PROCCES_CART_FAIL } from "../actions";
import { Product } from '../../models/product.model';

export interface CartState {
    cart: Product[];
    loaded: boolean;
    loading: boolean;
    error: any;
}

const initState: CartState = {
    cart: [],
    loaded: false,
    loading: false,
    error: null
}

export function cartReducer(state = initState, action: CartActions): CartState {
    switch (action.type) {
        case LOAD_CART:

            return {
                ...state,
                loading: true,
                error: null
            };
        case LOAD_CART_FAIL:

            return {
                ...state,
                loading: false,
                loaded: false,
                error: (<any>action).payload
            };
        case LOAD_CART_SUCCES:

            return {
                ...state,
                loading: false,
                loaded: true,
                cart: [...(<any>action).cart]
            };

        case ADD_PRODUCT_CART:

            return {
                ...state,
                error: null,
                loading: true,
                cart: [...state.cart, (<any>action).product]
            };

        case ADD_PRODUCT_CART_SUCCES:

            return {
                ...state,
                loading: false
            };

        case ADD_PRODUCT_CART_FAIL:

            return {
                ...state,
                loading: false,
                error: (<any>action).payload
            };

        case DELETE_PRODUCT_CART:
            return {
                ...state,
                error: null,
                loading: true,
                cart: [...state.cart.filter(data => data.id.localeCompare((<any>action).product.id) != 0)]
            };

        case DELETE_PRODUCT_CART_SUCCES:

            return {
                ...state,
                loading: false
            };

        case DELETE_PRODUCT_CART_FAIL:

            return {
                ...state,
                loading: false,
                error: (<any>action).payload
            };

        case PROCCES_CART:
            return {
                ...state,
                error: null,
                loading: true,
                cart: []
            };

        case PROCCES_CART_SUCCES:

            return {
                ...state,
                loading: false
            };

        case PROCCES_CART_FAIL:

            return {
                ...state,
                loading: false,
                error: (<any>action).payload
            };

        default:
            return state;
    }
}