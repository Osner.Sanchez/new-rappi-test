import { Injectable } from "@angular/core";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { switchMap, map, catchError } from "rxjs/operators";
import { of } from "rxjs";

import * as actions from '../actions'
import { LoadCategoriesSuccess, LoadCategoriesFail } from "../actions";
import { Category } from '../../models/category.model';
import { CategoriesService } from '../../services/categories/categories.service';

@Injectable()
export class CategoriesEffects {

    constructor(
        private actions$: Actions,
        private categoriesServices: CategoriesService
    ) { }

    @Effect()
    private loadCategories$ = this.actions$
        .pipe(
            ofType(actions.LOAD_CATEGORIES),
            switchMap(() => this.categoriesServices.getCategories()
                .pipe(
                    map((data: Category[]) => {
                        return new LoadCategoriesSuccess(data)
                    }),
                    catchError(error => of(new LoadCategoriesFail(error)))
                )
            )
        )

}