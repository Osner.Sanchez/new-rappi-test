import { ProductsEffects } from './products.effect';
import { CartEffects } from './cart.effect';
import { CategoriesEffects } from './categories.effect';


export const effectsArr: any[] = [ProductsEffects, CartEffects, CategoriesEffects];

export * from './products.effect'