import { Injectable } from "@angular/core";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { switchMap, map, catchError, withLatestFrom } from "rxjs/operators";
import { of } from "rxjs";

import * as actions from '../actions'
import { LoadProductsSuccess, LoadProductsFail } from "../actions";
import { ProductsService } from '../../services/products/products.service';
import { Product } from '../../models/product.model';
import { StateApp } from '../app.reducer';
import { Store } from '@ngrx/store';

@Injectable()
export class ProductsEffects {

  constructor(
    private actions$: Actions,
    private store: Store<StateApp>,
    private productsSerive: ProductsService
  ) { }

  @Effect()
  private loadProducts$ = this.actions$
    .pipe(
      ofType(actions.LOAD_PRODUCTS),
      withLatestFrom(this.store.select('products').pipe(map(store => store.filters))),
      switchMap(([action, filters]) => {

        return this.productsSerive.getProducts()
          .pipe(
            map((data: Product[]) => {

              if (filters) {
                let filterData: Product[] = [];
                if (filters.categories && filters.categories.length) {
                  filters.categories.forEach(category => {
                    filterData = [...filterData, ...data.filter(product => product.sublevel_id == category.id)]
                  });
                } else {
                  filterData = [...data]
                }
                if (filters.searchKey) {
                  filterData = [...filterData.filter(product => product.name.includes(filters.searchKey))];
                }
                if (filters.available) {
                  filterData = [...filterData.filter(product => product.available == filters.available)];
                }
                if (filters.minPrice) {
                  filterData = [...filterData.filter(product =>
                    parseFloat(product.price.replace('$', '').replace(',', '.')) >= filters.minPrice
                  )];
                }
                if (filters.maxPrice) {
                  filterData = [...filterData.filter(product =>
                    parseFloat(product.price.replace('$', '').replace(',', '.')) <= filters.maxPrice
                  )];
                }
                if (filters.minStock) {
                  filterData = [...filterData.filter(product =>
                    product.quantity >= filters.minStock
                  )];
                }
                return new LoadProductsSuccess(this.order(filterData, filters.order))
              }
              return new LoadProductsSuccess(this.order(data))
            }),
            catchError(error => of(new LoadProductsFail(error)))
          )
      })
    )

  order(array, order?) {
    switch (order) {
      case 'price':
        return array.sort((a, b) => parseFloat(a.price.replace('$', '').replace(',', '.')) - parseFloat(b.price.replace('$', '').replace(',', '.')))
      case 'available':
        return array.sort((a, b) => a.available - b.available)
      case 'quantity':
        return array.sort((a, b) => a.quantity - b.quantity)
      default:
        return array
    }
  }

}