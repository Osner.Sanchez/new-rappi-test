import { Injectable } from "@angular/core";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { switchMap, map, catchError, withLatestFrom } from "rxjs/operators";
import { of } from "rxjs";

import * as actions from '../actions'
import { AddProductCartSuccess, AddProductCartFail, LoadCartSuccess, LoadCartFail, DeleteProductCartSuccess, DeleteProductCartFail, ProccesCartSuccess, ProccesCartFail } from "../actions";
import { CookiesService } from '../../services/cookies/cookies.service';
import { StateApp } from '../app.reducer';
import { Store } from '@ngrx/store';
import { MatSnackBar } from '@angular/material';

@Injectable()
export class CartEffects {

    constructor(
        private actions$: Actions,
        private store: Store<StateApp>,
        private _snackBar: MatSnackBar,
        private cookieService: CookiesService
    ) { }

    @Effect()
    private loadCart$ = this.actions$
        .pipe(
            ofType(actions.LOAD_CART),
            switchMap(() => this.cookieService.getCookie('cart')
                .pipe(
                    map((data: string) => {
                        return new LoadCartSuccess(JSON.parse(data))
                    }),
                    catchError(error => of(new LoadCartFail(error)))
                )
            )
        )

    @Effect()
    private addProductCart$ = this.actions$
        .pipe(
            ofType(actions.ADD_PRODUCT_CART),
            withLatestFrom(this.store.select('cart').pipe(map(store => store.cart))),
            switchMap(([action, cart]) => {
                return of(this.cookieService.setCookie('cart', JSON.stringify(cart), 24)).pipe(
                    map(() => {
                        this.openSnackBar("Producto agregado al carrito", "x")
                        return new AddProductCartSuccess()
                    }),
                    catchError(error => of(new AddProductCartFail(error)))
                )
            })
        )
    @Effect()
    private deleteProductCart$ = this.actions$
        .pipe(
            ofType(actions.DELETE_PRODUCT_CART),
            withLatestFrom(this.store.select('cart').pipe(map(store => store.cart))),
            switchMap(([action, cart]) => {
                return of(this.cookieService.setCookie('cart', JSON.stringify(cart), 24)).pipe(
                    map(() => {
                        this.openSnackBar("Producto eleminado del carrito", "x")
                        return new DeleteProductCartSuccess()
                    }),
                    catchError(error => of(new DeleteProductCartFail(error)))
                )
            })
        )

    @Effect()
    private proccesCart$ = this.actions$
        .pipe(
            ofType(actions.PROCCES_CART),
            switchMap(() => {
                return of(this.cookieService.setCookie('cart', JSON.stringify([]), 24)).pipe(
                    map(() => {
                        this.openSnackBar("Compra procesada", "x")
                        return new ProccesCartSuccess()
                    }),
                    catchError(error => of(new ProccesCartFail(error)))
                )
            })
        )

    openSnackBar(message: string, action: string) {
        this._snackBar.open(message, action, {
            duration: 2000,
            panelClass: ['succes-snackbar']
        });
    }
}
