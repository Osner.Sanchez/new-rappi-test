import { Injectable } from '@angular/core';
import { of, Observable } from 'rxjs';
import { delay } from 'rxjs/operators';
import { categories } from '../../mocks/categories.json'

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  constructor() { }

  getCategories(): Observable<any> {
    return of(categories).pipe(
      delay(800) 
    );
  }
}
