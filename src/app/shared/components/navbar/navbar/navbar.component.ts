import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable, Subscription } from 'rxjs';
import { map, debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { Router, NavigationEnd } from '@angular/router';
import { StateApp } from 'src/app/shared/store/app.reducer';
import { Store } from '@ngrx/store';
import { MatDialog } from '@angular/material';
import { LoadProducts, LoadCategories } from 'src/app/shared/store/actions';
import { FormControl, Validators } from '@angular/forms';
import { Category } from 'src/app/shared/models/category.model';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  public filter = null;

  public road = {
    road: null,
    current: []
  };
  public selectCategory: Category = null;
  public categories: Category[] = [];
  public searchKey: FormControl = new FormControl('')
  public order: FormControl = new FormControl('')
  public available: FormControl = new FormControl(false)
  public maxPrice: FormControl = new FormControl('')
  public minPrice: FormControl = new FormControl('')
  public minStock: FormControl = new FormControl('')

  public viewSidenav = false;
  public quantityCart = 0;
  private refNativagate = new Subscription();
  public url: string = "";

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  constructor(
    private breakpointObserver: BreakpointObserver,
    private store: Store<StateApp>,
    private router: Router
  ) {

  }

  ngOnInit() {
    this.searchKey.valueChanges.pipe(
      debounceTime(200),
      distinctUntilChanged()
    ).subscribe(val => {
      this.addFilter({ searchKey: val })
    });

    this.available.valueChanges.pipe(
      debounceTime(200),
      distinctUntilChanged()
    ).subscribe(val => {
      this.addFilter({ available: val })
    });
    this.maxPrice.valueChanges.pipe(
      debounceTime(200),
      distinctUntilChanged()
    ).subscribe(val => {
      this.addFilter({ maxPrice: val })
    });

    this.minPrice.valueChanges.pipe(
      debounceTime(200),
      distinctUntilChanged()
    ).subscribe(val => {
      this.addFilter({ minPrice: val })
    });

    this.minStock.valueChanges.pipe(
      debounceTime(200),
      distinctUntilChanged()
    ).subscribe(val => {
      this.addFilter({ minStock: val })
    });

    this.order.valueChanges.pipe(
      debounceTime(200),
      distinctUntilChanged()
    ).subscribe(val => {
      this.addFilter({ order: val })
    });

    this.store.select('cart').subscribe(data => {
      this.quantityCart = data.cart.length;
    });
    this.validateLogin()
    this.refNativagate = this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.validateLogin()
      }
    });
    this.store.select("categories").subscribe(res => {
      this.categories = res.categories;
      this.road.current = res.categories
    });

    this.store.dispatch(new LoadCategories())

  }

  ngOnDestroy() {
    this.refNativagate.unsubscribe();
  }

  validateLogin() {
    this.url = this.router.url;
    // if(this.url === '/login' || !this.loginManager.isAuth()){
    if (this.url === '/login' || !true) {
      this.viewSidenav = false;
    } else {
      this.viewSidenav = true;
    }
  }

  logout() {

  }

  addFilter(filter: any) {
    this.filter = { ...this.filter, ...filter }
    this.store.dispatch(new LoadProducts({ ...this.filter, ...filter }))
  }

  nextCategory(category: Category) {

    if (category.sublevels) {
      this.road = {
        road: this.road,
        current: category.sublevels
      }
    } else {
      this.selectCategory = category;
      this.addFilter({ categories: [category] });

    }
  }

  backCategory() {
    this.road = {
      current: this.road.road.current,
      road: this.road.road.road
    }
  }

}
