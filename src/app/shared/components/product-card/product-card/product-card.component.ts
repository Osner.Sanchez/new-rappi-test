import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Product } from 'src/app/shared/models/product.model';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.scss']
})
export class ProductCardComponent implements OnInit {

  @Input()
  product: Product

  @Output()
  onAddCart: EventEmitter<Product> = new EventEmitter<Product>()

  public active: boolean = false;
  public quantity: FormControl = new FormControl(1)

  constructor() { }

  ngOnInit() {
  }

  addCart() {
    this.onAddCart.emit({ ...this.product, quantity: this.quantity.value });
  }

}
