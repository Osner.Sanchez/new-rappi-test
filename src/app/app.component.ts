import { Component } from '@angular/core';
import { ProductsService } from './shared/services/products/products.service';
import { CategoriesService } from './shared/services/categories/categories.service';
import { StateApp } from './shared/store/app.reducer';
import { Store } from '@ngrx/store';
import { LoadCart } from './shared/store/actions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(
    private store: Store<StateApp>
  ) { 
    this.store.dispatch(new LoadCart())
  }
  title = 'new-rappi-test';
}
