import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PrincipalLayoutComponent } from './layouts/principal-layout/principal-layout.component';
import { UserLayoutComponent } from './layouts/user-layout/user-layout.component';
import { AuthGuard } from './shared/guards/auth/auth.guard';


const routes: Routes = [
  { path: '', redirectTo: 'index', pathMatch: 'full' },
  {
    path: '',
    component: UserLayoutComponent,
    // canLoad: [AuthGuard],
    canActivate: [AuthGuard],
    children: [
      { path: '', loadChildren: './user/user.module#UserModule' }
    ]
  }, {
    path: '',
    component: PrincipalLayoutComponent,
    children: [
      { path: '', loadChildren: './principal/principal.module#PrincipalModule' },
    ]
  }, {
    path: '**',
    redirectTo: 'index'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
