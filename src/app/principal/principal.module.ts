import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ScrollingModule } from '@angular/cdk/scrolling';

import { PrincipalRoutingModule } from './principal-routing.module';
import { HomeComponent } from './home/home.component';
import { ProductCardModule } from '../shared/components/product-card/product-card.module';
import { ShoppingCartComponent } from './shopping-cart/shopping-cart.component';
import { MatTableModule, MatPaginatorModule, MatCardModule, MatMenuModule, MatIconModule, MatButtonModule } from '@angular/material';

@NgModule({
  declarations: [HomeComponent, ShoppingCartComponent],
  imports: [
    CommonModule,
    MatCardModule,
    MatTableModule,
    MatButtonModule,
    MatMenuModule,
    MatIconModule,
    MatPaginatorModule,
    PrincipalRoutingModule,
    ScrollingModule,
    ProductCardModule
  ]
})
export class PrincipalModule { }
