import { Component, OnInit, ViewChild } from '@angular/core';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { Product } from 'src/app/shared/models/product.model';
import { Store } from '@ngrx/store';
import { StateApp } from 'src/app/shared/store/app.reducer';
import { DeleteProductCart, ProccesCart } from 'src/app/shared/store/actions';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.scss']
})
export class ShoppingCartComponent implements OnInit {


  public dataSource: MatTableDataSource<any[]> = new MatTableDataSource([]);
  public loading: boolean = false
  public list: any[] = [];
  public displayedColumns: any[] = [
    { def: 'options', showMobile: true }, //
    { def: 'name', showMobile: true },//
    { def: 'price', showMobile: true }, //
    { def: 'quantity', showMobile: true }, //
    { def: 'total', showMobile: true } //
  ];

  public isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  @ViewChild(MatSort, { static: true }) set matSort(ms: MatSort) {
    this.dataSource.sort = ms;
  }

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(
    private store: Store<StateApp>,
    private breakpointObserver: BreakpointObserver
  ) { }

  ngOnInit() {
    this.store.select('cart').subscribe(data => {
      this.list = data.cart;
      this.changeData();
    })
  }

  ngAfterViewInit() {
  }

  changeData() {
    this.dataSource = new MatTableDataSource(this.list);
    setTimeout(() => {
      this.dataSource.paginator = this.paginator;
    }, 1);
  }

  getDisplayedColumns(isMobile): string[] {
    return this.displayedColumns
      .filter(cd => !isMobile || cd.showMobile)
      .map(cd => cd.def);
  }

  getTotal() {
    let amounts = [...this.list.map(data => parseFloat(data.price.replace('$', '').replace(',', '.')) * data.quantity)]
    return amounts.length ? amounts.reduce((a, b) => a + b) : 0;
  }

  delete(row) {
    this.store.dispatch(new DeleteProductCart(row))
  }

  procces() {
    this.store.dispatch(new ProccesCart())
  }

  edit() {

  }

}
