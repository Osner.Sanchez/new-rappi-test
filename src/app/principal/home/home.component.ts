import { Component, OnInit } from '@angular/core';
import { StateApp } from 'src/app/shared/store/app.reducer';
import { Store } from '@ngrx/store';
import { LoadProducts, AddProductCart } from 'src/app/shared/store/actions';
import { Product } from 'src/app/shared/models/product.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public products: Product[] = [];

  constructor(
    private store: Store<StateApp>
  ) { }

  ngOnInit() {

    this.store.select('products').subscribe(data => {
      this.products = data.products;
    });

    this.store.dispatch(new LoadProducts())
  }

  addCart(product: Product) {
    this.store.dispatch(new AddProductCart(product))
  }

}
